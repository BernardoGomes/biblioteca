# -*- coding: utf-8 -*-


from django.views.generic import TemplateView
from django.contrib.messages.views import SuccessMessageMixin

from django.views.generic import ListView
from django.views.generic.edit import CreateView

from apps.library.models import UserLibrary
from apps.library.models import Book
from apps.library.models import Borrowing
from apps.library.forms import UserLibraryForm
from apps.library.forms import BookForm
from apps.library.forms import BorrowingForm
from apps.library.serializers import UserLibrarySerializer
from apps.library.serializers import BookSerializer
from rest_framework.generics import ListAPIView
from rest_framework.renderers import JSONRenderer


class Index(TemplateView):
    template_name = "library/index.html"

    def get_context_data(self, **kwargs):
        context = super(Index, self).get_context_data(**kwargs)
        context['title'] = context['pageHeader'] = "Dashboard"
        context['showBtnBack'] = False
        return context


class UserLibraryCreate(SuccessMessageMixin, CreateView):
    model = UserLibrary
    form_class = UserLibraryForm
    success_message = "%(name)s foi criado com sucesso."

    def get_context_data(self, **kwargs):
        context = super(UserLibraryCreate, self).get_context_data(**kwargs)
        context['title'] = context['pageHeader'] = "Novo usuário"
        context['showBtnBack'] = True
        return context


class UserLibraryList(ListView):
    model = UserLibrary
    paginate_by = 5

    def get_context_data(self, **kwargs):
        context = super(UserLibraryList, self).get_context_data(**kwargs)
        context['title'] = context['pageHeader'] = "Lista de usuários"
        context['showBtnBack'] = True
        return context


class UserLibraryListApi(ListAPIView):
    renderer_classes = (JSONRenderer, )
    queryset = UserLibrary.objects.all()
    serializer_class = UserLibrarySerializer


class BookListApi(ListAPIView):
    renderer_classes = (JSONRenderer, )
    queryset = Book.objects.all()
    serializer_class = BookSerializer


class BookCreate(SuccessMessageMixin, CreateView):
    model = Book
    form_class = BookForm
    success_message = "%(name)s foi criado com sucesso."

    def get_context_data(self, **kwargs):
        context = super(BookCreate, self).get_context_data(**kwargs)
        context['title'] = context['pageHeader'] = "Novo livro"
        context['showBtnBack'] = True
        return context


class BookList(ListView):
    model = Book
    paginate_by = 5

    def get_context_data(self, **kwargs):
        context = super(BookList, self).get_context_data(**kwargs)
        context['title'] = context['pageHeader'] = "Lista de livros"
        context['showBtnBack'] = True
        return context


class BorrowingCreate(CreateView):
    model = Borrowing
    form_class = BorrowingForm

    success_message = "O empréstimo foi criado com sucesso."

    def form_valid(self, form):
        self.object = form.save()
        self.object.book.is_borrowed = True
        self.object.book.save()
        return super(BorrowingCreate, self).form_valid(form)

    def get_context_data(self, **kwargs):
        context = super(BorrowingCreate, self).get_context_data(**kwargs)
        context['title'] = context['pageHeader'] = "Empréstimos de livro"
        context['showBtnBack'] = True
        return context


class BorrowingList(ListView):
    model = Borrowing
    paginate_by = 5

    def get_context_data(self, **kwargs):
        context = super(BorrowingList, self).get_context_data(**kwargs)
        context['title'] = context['pageHeader'] = "Lista de empréstimos"
        context['showBtnBack'] = True
        return context
