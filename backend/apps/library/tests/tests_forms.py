# -*- coding: utf-8 -*-


from django.test import TestCase
from apps.library.forms import UserLibraryForm
from apps.library.forms import BookForm
from apps.library.forms import BorrowingForm
from apps.library.models import UserLibrary
from apps.library.models import Book
from apps.library.models import Borrowing


class UserLibraryFormTest(TestCase):
    def test_forms_validate(self):
        form_data = {'brasil': 'argentina'}
        form = UserLibraryForm(data=form_data)
        self.assertFalse(form.is_valid())

        form_data = {'name': 'argentina'}
        form = UserLibraryForm(data=form_data)
        self.assertTrue(form.is_valid())

    def test_forms_data(self):
        form_data = {'name': 'argentina'}
        form = UserLibraryForm(data=form_data)
        self.assertTrue(form.is_valid())
        form.save()
        u = UserLibrary.objects.last()
        self.assertTrue(u.name == form_data['name'])

    def test_form_create_without_name(self):
        form_data = {'name': ''}
        form = UserLibraryForm(data=form_data)
        self.assertFalse(form.is_valid())


class BookFormTest(TestCase):
    def test_forms_validate(self):
        form_data = {'brasil': 'argentina'}
        form = BookForm(data=form_data)
        self.assertFalse(form.is_valid())

        form_data = {'name': 'argentina'}
        form = BookForm(data=form_data)
        self.assertTrue(form.is_valid())

    def test_forms_data(self):
        form_data = {'name': 'argentina'}
        form = BookForm(data=form_data)
        form.save()
        self.assertTrue(form.is_valid())
        u = Book.objects.last()
        self.assertTrue(u.name == form_data['name'])

    def test_form_create_without_name(self):
        form_data = {'name': ''}
        form = BookForm(data=form_data)
        self.assertFalse(form.is_valid())


class BorrowingFormTest(TestCase):
    def test_forms_validate(self):
        form_data = {'brasil': 'argentina'}
        form = BorrowingForm(data=form_data)
        self.assertFalse(form.is_valid())
        u = UserLibrary()
        u.name = 'ber'
        u.save()
        b = Book()
        b.name = 'b123'
        b.save()
        form_data = {'book': str(b.id), 'user': str(u.id)}
        form = BorrowingForm(data=form_data)
        self.assertTrue(form.is_valid())

    def test_forms_data(self):
        u = UserLibrary()
        u.name = 'ber'
        u.save()
        b = Book()
        b.name = 'b123'
        b.save()
        form_data = {'book': str(b.id), 'user': str(u.id)}
        form = BorrowingForm(data=form_data)
        form.save()
        bor = Borrowing.objects.last()
        self.assertTrue(str(bor.book.id)==form_data['book'] and str(bor.user.id)==form_data['user'])
