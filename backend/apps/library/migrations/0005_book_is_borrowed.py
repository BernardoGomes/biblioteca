# -*- coding: utf-8 -*-
# Generated by Django 1.11.5 on 2017-10-05 02:00
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('library', '0004_auto_20171005_0048'),
    ]

    operations = [
        migrations.AddField(
            model_name='book',
            name='is_borrowed',
            field=models.BooleanField(default=False),
        ),
    ]
