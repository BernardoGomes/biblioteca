# urls.py
from django.conf.urls import url

from apps.library.views import UserLibraryCreate
from apps.library.views import UserLibraryList
from apps.library.views import UserLibraryListApi
from apps.library.views import BookCreate
from apps.library.views import BookList
from apps.library.views import BookListApi
from apps.library.views import BorrowingCreate
from apps.library.views import BorrowingList
from apps.library.views import Index

urlpatterns = [
    url(r'new-user/$', UserLibraryCreate.as_view(), name='new-user'),
    url(r'new-book/$', BookCreate.as_view(), name='new-book'),
    url(r'users/$', UserLibraryList.as_view(), name='users-list'),
    url(r'books/$', BookList.as_view(), name='books-list'),
    url(r'users-api/$', UserLibraryListApi.as_view(), name='users-list-api'),
    url(r'books-api/$', BookListApi.as_view(), name='books-list-api'),
    url(r'new-borrowing/$', BorrowingCreate.as_view(), name='new-borrowing'),
    url(r'borrowings/$', BorrowingList.as_view(), name='borrowings-list'),
    url(r'dashboard/$', Index.as_view(), name='dashboard'),

]
