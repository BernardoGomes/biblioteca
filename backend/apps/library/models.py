# -*- coding: utf-8 -*-

from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.urls import reverse


class UserLibrary(models.Model):

    name = models.CharField(_('Nome'), max_length=50, blank=False, null=False,
                            unique=True)
    # author TODO  with unique together with name

    class Meta:
        verbose_name = "Usuário"
        verbose_name_plural = "Users"

    def __str__(self):
        return "{} - {}".format(self.id, self.name)

    def get_absolute_url(self):
        return reverse('library:users-list')


class Book(models.Model):
    name = models.CharField(_('Nome'), max_length=50, blank=False, null=False,
                            unique=True)
    is_borrowed = models.BooleanField(default=False)

    class Meta:
        verbose_name = "Livro"
        verbose_name_plural = "Books"

    def __str__(self):
        return "{} - {}".format(self.id, self.name)

    def get_absolute_url(self):
        return reverse('library:books-list')


class Borrowing(models.Model):
    book = models.ForeignKey(Book)
    user = models.ForeignKey(UserLibrary)

    initial_date = models.DateTimeField(auto_now_add=True, blank=False,
                                        null=False)
    finish_date = models.DateTimeField(blank=False, null=True)

    class Meta:
        verbose_name = "Empréstimo"
        verbose_name_plural = "Borrowings"

    def __str__(self):
        return "{} - {} - {}".format(self.book, self.user, '')

    def get_absolute_url(self):
        return reverse('library:borrowings-list')

