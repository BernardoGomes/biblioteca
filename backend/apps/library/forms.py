# -*- coding: utf-8 -*-
from django import forms
from django.forms import ModelForm
from apps.library.models import UserLibrary
from apps.library.models import Book
from apps.library.models import Borrowing


class UserLibraryForm(ModelForm):
    name = forms.CharField(label=("Nome"),
                           widget=forms.TextInput(attrs={'class': 'form-control'}))

    class Meta:
        model = UserLibrary
        fields = ['name']


class BookForm(ModelForm):
    name = forms.CharField(label=("Nome"),
                           widget=forms.TextInput(attrs={'class': 'form-control'}))

    class Meta:
        model = Book
        fields = ['name']


class BorrowingForm(ModelForm):
    book = forms.ModelChoiceField(queryset=Book.objects.all(), label=("Selecione o livro"),
                                  widget=forms.Select(attrs={'class': 'form-control'}))
    user = forms.ModelChoiceField(queryset=UserLibrary.objects.all(), label=("Selecione o usuário"),
                                  widget=forms.Select(attrs={'class': 'form-control'}))

    class Meta:
        model = Borrowing
        fields = ['book', 'user']

    def clean_book(self):
        book = self.cleaned_data['book']

        if book.is_borrowed:
            raise forms.ValidationError("Livro indisponível.")

        return book